import React from "react";
import {tokenExists,getParameterByName} from "./TokenHandler";
import App from "./App";
import {config} from "./Constants/Constants";
import {loginDemoStore} from "./Stores/LoginDemoStore";

class LogIn extends React.Component{
    constructor() {
        super();
        this.state = {
            loggedIn : false
        };
        this.handleLogin = this.handleLogin.bind(this)
    }

    componentDidMount() {
        console.log("mounted")
        loginDemoStore.fetchLogin()
        const token = getParameterByName("token");
        if (token != null && token.length > 0) {
            localStorage.setItem("portal-jwt-Token", token);
            if(tokenExists()) {
                this.setState({
                    loggedIn : true
                })
            }
        }
        if(tokenExists()) {
            this.setState({
                loggedIn : true
            })
        }
    }
    componentDidUpdate(prevProps, prevState, snapshot) {
        console.log("updated")
    }

    handleLogin(){
        const url1 = config.url.API_URL+"/login"
        window.location.replace(url1)
    }

    render() {
        const isLoggedIn = this.state.loggedIn;
        let element;
        if(isLoggedIn){
            console.log("inside app")
            element = (
                <div>
                    <App/>
                </div>
            )
        }else {
            console.log("inside login")
            element = (
                <div className="card text-center">
                    <div className="card-header">
                    </div>
                    <div className="card-body">
                        <h5 className="card-title">Information</h5>
                        <p className="card-text">Du skal logge ind med DTU campus net for at komme ind på Diplom portal</p>
                        <a href="#" onClick={()=>{this.handleLogin()}} className="btn btn-primary">Login med Campus net</a>
                    </div>
                    <div className="card-body">
                        <a href="#" onClick={()=>{localStorage.setItem("portal-jwt-Token", loginDemoStore.token.jwt); window.location.replace("/")}} className="btn btn-primary">Login demo</a>
                    </div>
                    <div className="card-footer text-muted">
                    </div>
                </div>
            )
        }
        return (
           element
        )
    }
}
export default LogIn;