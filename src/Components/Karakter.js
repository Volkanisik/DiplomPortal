import React from "react";
import {gradesStore} from "../Stores/GradesStore";
import {observer} from "mobx-react";
import {Button, Grid} from "@material-ui/core";
import {Box, Container, Paper, Typography} from "@mui/material";
import {config} from "../Constants/Constants";
import {navigationStore} from "../Stores/NavigationStore";

class Karakter extends React.Component {
    constuctor() {
        this.routeChange = this.routeChange.bind(this);
    }

    routeChange(newPath) {
        this.props.history.push(newPath);
    }

    componentDidMount() {
        gradesStore.fetchTakes()
    }


    render() {
        navigationStore.updatePage(2)
        function getPdf() {
            const myHeaders = new Headers();
            myHeaders.append("Authorization", "Bearer " + localStorage.getItem("portal-jwt-Token"));

            const requestOptions = {
                method: 'GET',
                headers: myHeaders,
                redirect: 'follow'
            };

            fetch(config.url.API_URL+"/students/export", requestOptions)
                .then(response => response.blob())
                .then(blob => {
                    const url = window.URL.createObjectURL(new Blob([blob]));
                    const link = document.createElement('a');
                    link.href = url;
                    link.setAttribute('download', 'grades.pdf');
                    document.body.appendChild(link);
                    link.click();
                    link.parentNode.removeChild(link);
                })
                .catch(error => console.log('error', error));
        }

        return (
            <Box>
                <Box
                    sx={{
                        display: 'flex',
                        flexDirection: 'row-reverse',
                        p: 1,
                        m: 1
                    }}
                >
                    <Button variant="contained" onClick={getPdf}
                            style={{backgroundColor: "#219160", color: "white"}}>Pdf</Button>
                </Box>
                <Container>
                    <Grid container spacing={10}>
                        {gradesStore.semesters.map((semester) =>
                            <Grid key={semester.year} item xs={12} md={6}>
                                <Paper style={{
                                    backgroundColor: "lightgrey",
                                    m: 1,

                                }}>
                                    <Box
                                        sx={{
                                            display: 'flex',
                                            justifyContent: 'flex-start',
                                            position: "relative"
                                        }}
                                    >
                                        <Typography variant="h6" align="left" sx={{
                                            padding: "5px",
                                            position: "absolute",
                                            top: "-35px"
                                        }}>
                                            {semester.nr}.Semester
                                        </Typography>
                                    </Box>
                                    <Box sx={{padding: "20px"}}>
                                        {semester.courses.map((course) =>
                                            <Grid key={course.courses_id} container spacing={0}>
                                                <Grid item xs={8} style={{padding: "10px"}}>
                                                    <Paper style={{backgroundColor: "darkgray"}}>
                                                        <Typography align="left" sx={{ml: "5px"}}>
                                                            {course.courses_id}
                                                        </Typography>
                                                        <Typography variant="h6" align="left" sx={{ml: "5px"}}>
                                                            {course.name}
                                                        </Typography>
                                                        <Typography align="left" sx={{ml: "5px"}}>
                                                            <strong> {course.ects} ECTS</strong>
                                                        </Typography>
                                                    </Paper>
                                                </Grid>
                                                <Grid item xs={4} style={{padding: "10px"}}>
                                                    <Paper sx={{
                                                        display: "flex",
                                                        alignItems: "center",
                                                        justifyContent: "center"
                                                    }} style={{backgroundColor: "darkgray", height: "100%"}}>
                                                        <Typography variant="h5" align="center" sx={{ml: "5px"}}>
                                                            {course.grade}
                                                        </Typography>
                                                    </Paper>
                                                </Grid>
                                            </Grid>
                                        )}
                                    </Box>
                                </Paper>
                            </Grid>
                        )}
                    </Grid>
                </Container>
            </Box>
        )
    }
}

export default observer(Karakter);