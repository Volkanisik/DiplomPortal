import React from "react";
import {Grid, Paper} from "@material-ui/core";
import {observer} from "mobx-react";
import Typography from "@mui/material/Typography";
import {createTheme} from '@mui/material/styles';
import {ThemeProvider} from "@emotion/react";
import {Box} from "@mui/material";
import CourseStore from "../Stores/CourseStore";
import Semester from "./Semester";
import {navigationStore} from "../Stores/NavigationStore";
// src: https://mui.com/components/grid/

let courseTheme = createTheme();

courseTheme.typography.h5 = {
    fontSize: '0.9rem',
    '@media (min-width:600px)': {
        fontSize: '1.0rem',
    },
    [courseTheme.breakpoints.up('md')]: {
        fontSize: '1.2rem',
    },
}
const kalenderstore = new CourseStore();

class Kurser extends React.Component {
    constuctor() {
        this.routeChange = this.routeChange.bind(this);
    }

    routeChange(newPath) {
        this.props.history.push(newPath);
    }
    componentDidMount() {
        kalenderstore.fetchWeeks()
    }

    render() {
        navigationStore.updatePage(0)
        return (
            <ThemeProvider theme={courseTheme}>
        <Semester store={kalenderstore} />

                <h1 className="center" style={{paddingTop:"1%",paddingBottom:"2%"}}>Courses</h1>
                <Grid container spacing={2}>
                    {kalenderstore.currentSemester.map((course) =>
                        <Grid key={course.courses_id} item xs={12} sm={6} md={4}>

                                <Box
                                    display="flex"
                                    alignItems="center"
                                    justifyContent="center"
                                >
                                <Paper   style={{height:"250px", width:"80%",backgroundColor:"lightgray"}} onClick={()=>{
                                    this.routeChange("/course/"+course.courses_id+"/"+course.year)
                                }}>
                                <Box  sx={{
                                    height:"100%",
                                    width:"100%"
                                }}>
                                    <Box sx={{
                                        height:"35%",
                                        width:"100%"
                                    }}>
                                    <Typography variant="h5" >
                                        <b>{course.courses_id}</b>
                                    </Typography>

                                    <Typography variant="h5">
                                        <b>{course.name}</b>
                                    </Typography>
                                    </Box>
                                    <Box sx={{
                                        height:"65%",
                                        width:"100%",
                                    }}>
                                    <img className="image"
                                         alt={course.name}
                                         src={course.image}
                                         style={{width:"90%",height:"80%"}}
                                    />
                                    </Box>
                                </Box>
                                </Paper>
                                </Box>


                        </Grid>
                    )}
                </Grid>
            </ThemeProvider>
        )
    }
}

export default observer(Kurser);