import React from "react";
import {Container, Nav, Navbar, Modal, Button} from "react-bootstrap";
import './NavigationBar.css'
import jwtDecode from "jwt-decode";
import person from '../../person-circle.svg'
import {logout} from "../../TokenHandler";
import {Box} from "@material-ui/core";
import {Link} from 'react-router-dom'
import {weatherStore} from "../../Stores/WeatherStore.js";
import {observer} from "mobx-react";
import {navigationStore} from "../../Stores/NavigationStore";

class NavigationBar extends React.Component{
    constructor(props) {
        super(props);
        this.state={ showPopUp: false }
    }

    componentDidMount() {
        weatherStore.fetchWeatherData();
    }

    render() {
        const studentId = () => {
            const token = localStorage.getItem("portal-jwt-Token")
            try {
                const decoded = jwtDecode(token)
                console.log(decoded)
                return decoded.sub;
            }catch (e) {

            }
        }
        const openPopUp = () => {
            console.log("open popup")
            this.setState({showPopUp:true})
        }
        const closePopUp = () =>{
            this.setState({showPopUp:false})
        }
        const onLogOut = () =>{
            logout()
        }
        const onInstall = () =>{
            console.log("Install app");
            window.deferredPrompt.prompt();
        }

        return(

            <div>
                <Navbar expand="lg" style={{backgroundColor:"#dadada"}}>
                    <Container fluid className="mx-4">

                        {/*Logo*/}
                        <Navbar.Brand href="/">
                            <img alt="DTU logo" src="https://i.kamel.devops.diplomportal.dk/DTU-tight.png"
                                 height="70"/>
                        </Navbar.Brand>

                        {/*Toggle*/}
                        <Navbar.Toggle aria-controls="navbarScroll" />

                        {/*Titles*/}
                        <Navbar.Collapse id="navbarScroll">
                            <Nav className="me-auto my-2 my-lg-0">
                                <Nav.Link as={Link}
                                          to="/"
                                          className="h5"
                                          style={
                                              navigationStore.page === 0 ? {fontWeight: "bold"} : {fontWeight: "normal"}
                                          }
                                          onClick={() => navigationStore.updatePage(0)}>Kurser</Nav.Link>
                                <Nav.Link as={Link}
                                          to="/kalender"
                                          className="h5"
                                          style={
                                              navigationStore.page === 1 ? {fontWeight: "bold"} : {fontWeight: "normal"}
                                          }
                                          onClick={() => navigationStore.updatePage(1)}>Kalender</Nav.Link>
                                <Nav.Link as={Link}
                                          to="/karakter"
                                          className="h5"
                                          style={
                                              navigationStore.page === 2 ? {fontWeight: "bold"} : {fontWeight: "normal"}
                                          }
                                          onClick={() => navigationStore.updatePage(2)}>Karakter</Nav.Link>
                                <Nav.Link as={Link}
                                          to="/studieplanlaegger"
                                          className="h5"
                                          style={
                                              navigationStore.page === 3 ? {fontWeight: "bold"} : {fontWeight: "normal"}
                                          }
                                          onClick={() => navigationStore.updatePage(3)}>StudiePlanlægger</Nav.Link>
                            </Nav>


                            {/*User profile*/}
                            <Box>
                                <Navbar.Collapse className="row">
                                    <Modal show={this.state.showPopUp}>
                                        <Modal.Header>
                                            Vil du Logge ud?
                                        </Modal.Header>
                                        <Modal.Body>
                                            <div className="row">
                                                <Button onClick={onLogOut} className="btn-danger">Log ud</Button>
                                                <Button onClick={onInstall} variant="info">Installer PWA!</Button>
                                                <Button onClick={closePopUp} className="btn-close-white">Annuller</Button>
                                            </div>
                                        </Modal.Body>
                                    </Modal>
                                    <img src={person} onClick={openPopUp} alt="person Logo" width="30" height="30"/>
                                    <text className="">{studentId()}</text>
                                </Navbar.Collapse>
                            </Box>
                        </Navbar.Collapse>
                    </Container>
                </Navbar>

                {/*Weather*/}
                {/* true && expression always evaluates to expression, and false && expression always evaluates to false */}
                { (weatherStore.temperature != null && weatherStore.weatherCode != null) &&
                <div className="mx-4" style={{
                    display: "flex",
                    alignItems: 'left',
                    justifyContent: 'left',
                    columnGap: 10,
                    marginTop: 10
                }}>
                    <img alt="DTU logo" src={"https://i.kamel.devops.diplomportal.dk/" + weatherStore.weatherCode + ".png"}
                         height="30"/>
                    <text >{weatherStore.temperature} °C</text>
                </div>
                }

            </div>
        )
    }
}

export default observer(NavigationBar);
