import {Draggable, Droppable} from "react-beautiful-dnd";
import Box from "@mui/material/Box";
import {Typography} from "@mui/material";
import Paper from "@mui/material/Paper";
import {ThemeProvider} from "@emotion/react";
import CourseCard from "./SemesterCourseCard";
import React from "react";
import {createTheme, styled} from "@mui/material/styles";
import {observer} from "mobx-react";

const colors = () => {
    return ({
        Red: "#990000",
        Darkgrey: "#C4C4C4",
        Mediumgrey: "#D9D9D9",
        Lightgrey: "#ECECEC"
    })
}
function KurserList({coursedata}) {
    let courseTheme = createTheme();
    courseTheme.typography.h5 = {
        fontSize: '0.9rem',
        '@media (min-width:600px)': {
            fontSize: '1.0rem',
        },
        [courseTheme.breakpoints.up('md')]: {
            fontSize: '1.2rem',
        },
    }

    const StyleBox = styled(Box)(({theme}) => ({
        [theme.breakpoints.down('md')]: {
            display: 'flex',
            justifyContent: 'center'
        },
        [theme.breakpoints.up('md')]: {
            display: 'flex',
            justifyContent: 'start'
        },
        [theme.breakpoints.up('lg')]: {
            display: 'flex',
            justifyContent: 'start'
        },
    }));

    const StyleListOverlay = styled(Paper)(({theme}) => ({
        [theme.breakpoints.down('md')]: {
            maxWidth: "400px",
            maxHeight: "400px",
            position: "relative",
            borderRadius: '12px'
        },
        [theme.breakpoints.up('md')]: {
            maxWidth: "400px",
            minHeight: "100px",
            position:"fixed",
            mx: 'auto',
            height: "60%",
            backgroundColor: "lightgrey",
            borderRadius: '12px'
        },
        [theme.breakpoints.up('lg')]: {
            maxWidth: "400px",
            minHeight: "100px",
            position:"fixed",
            mx: 'auto',
            height: "60%",
            backgroundColor: "lightgrey",
            borderRadius: '12px'
        },
    }));

    const StyleList = styled(Paper)(({theme}) => ({
        [theme.breakpoints.down('md')]: {
            maxWidth: "400px",
            maxHeight: "200px",
            position: "relative",
            overflow: "scroll",
            borderRadius: '12px'
        },
        [theme.breakpoints.up('md')]: {
            maxWidth: "400px",
            minHeight: "100px",
            my: 1,
            mx: 'auto',
            p: 2,
            position: "fixed",
            height: "60%",
            overflow: "scroll",
            borderRadius: '12px'
        },
        [theme.breakpoints.up('lg')]: {
            maxWidth: "400px",
            minHeight: "100px",
            my: 1,
            mx: 'auto',
            p: 2,
            position: "fixed",
            height: "60%",
            overflow: "scroll",
            borderRadius: '12px'
        },
    }));

    return (
        <StyleBox>
            {Array.from(Array(1)).map((_, index) =>
                <Droppable droppableId={"mydroppable 2"}>
                    {
                        (provided, snapshot) => (
                            <StyleListOverlay>
                                <Box
                                    sx={{
                                        display: 'flex',
                                        justifyContent: 'flex-start',
                                        position: "relative"
                                    }}
                                >
                                    <Typography variant="h6" align="left" sx={{
                                        position: "absolute",
                                        top: "-35px"
                                    }}>
                                        <h4>Kurser</h4>
                                    </Typography>
                                </Box>
                                <StyleList ref={provided.innerRef}>
                                    <Paper
                                        data-testid = "custom-element"
                                        ref={provided.innerRef}
                                        style={{
                                            backgroundColor: colors().Lightgrey,
                                            borderRadius:12
                                        }}>
                                        <ThemeProvider theme={courseTheme}>
                                            <Box sx={{padding: "20px"}}>
                                                {
                                                    coursedata.map((course, index) => (
                                                            <Draggable
                                                                key={course.id.toString()}
                                                                draggableId={course.id.toString()}
                                                                index={index}>
                                                                {(provided) => (
                                                                    <div
                                                                        ref={provided.innerRef}
                                                                        {...provided.draggableProps}
                                                                        {...provided.dragHandleProps}
                                                                    >
                                                                        <CourseCard id={(course.id)}
                                                                                    name={course.name}
                                                                                    index={index}
                                                                                    day={course.day}
                                                                                    time={course.time}
                                                                                    ects={course.ects}
                                                                                    isNotDraggable={false}

                                                                        />
                                                                    </div>
                                                                )}
                                                            </Draggable>
                                                        )
                                                    )
                                                }
                                            </Box>
                                        </ThemeProvider>
                                        {provided.placeholder}
                                    </Paper>
                                </StyleList>
                            </StyleListOverlay>
                        )
                    }
                </Droppable>
            )}
        </StyleBox>
    )
}

export default observer(KurserList);