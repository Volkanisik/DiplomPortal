import React from "react";
import Typography from '@mui/material/Typography';
import {Grid, Paper} from "@material-ui/core";


export default function CourseCard(props) {
    const colors = () => {
        return ({
            Red: "#990000",
            Darkgrey: "#C4C4C4",
            Mediumgrey: "#D9D9D9",
            Lightgrey: "#ECECEC"
        })
    }
    const color = () => (props.isNotDraggable === true)?colors().Mediumgrey : colors().Darkgrey
    return(
        <Grid key={props.id} container spacing={0}>
            <Grid item xs={12} style={{padding:"20px"}}>
                <Paper data-testid = {"coursecard-element"+props.id} style={{backgroundColor: color(), borderRadius:10, padding:5,width:300}} id={123}>
                    <Grid key={props.id} container spacing={0}>
                    <Grid item xs={10}>
                        <Typography align="left" sx={{ml: "5px"}}>
                            {props.id}
                        </Typography>
                        <Typography variant="h6" align="left" sx={{ml: "5px"}}>
                            <strong> {props.name} </strong>
                        </Typography>
                        <Typography align="left" sx={{ml: "5px"}}>
                             {props.ects} ECTS
                        </Typography>
                    </Grid>
                    <Grid item xs={2}>
                        <Typography align="left" sx={{ml:-1, fontSize:11}} >
                            {props.day} kl.{(props.time==='MORNING')?8:13}
                        </Typography>
                    </Grid>
                    </Grid>
                </Paper>
            </Grid>
        </Grid>
    );
}