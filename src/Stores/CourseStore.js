import {makeObservable, observable, runInAction} from "mobx";
import {config} from "../Constants/Constants";
import fetchData from "./Utility";

const weekdanish = ["Mandag", "Tirsdag", "Onsdag", "Torsdag", "Fredag"]
export default class CourseStore {
    weeks = [];
    courses = [];
    semesters = [];
    currentSemester = [];
    weekdays = ["MON", "TUE", "WED", "THU", "FRI"]


    constructor() {
        makeObservable(this, {
            weeks: observable,
            weekdays: observable,
            courses: observable,
            semesters: observable,
            currentSemester: observable
        })
    }

    setweekdays() {
        const weekdanish = ["Mandag", "Tirsdag", "Onsdag", "Torsdag", "Fredag"]
        const weekdays = ["MON", "TUE", "WED", "THU", "FRI"]
        for (let i = 0; i < weekdays.length; i++) {
            let courseday = {
                day: weekdanish[i],
                morning: {id: " ", name: " ", day: " ", color: "lightgray"},
                lunch: {id: " ", name: " ", day: " ", color: "lightgray"},
            }
            runInAction(() => this.weeks.push(courseday))
        }
    }

    arrangeSemester(year) {

        let weekcourses = [];
        let i = 0;
        for (const days of this.weekdays) {
            let courseday = {
                day: weekdanish[i],
                coursemorning: {},
                courselunch: {},
                morning: {id: "", name: "", time: "", color: "lightgray"},
                lunch: {id: "", name: "", time: "", color: "lightgray"},
            }
            for (let course of this.courses) {
                if (days === course.day && course.year.toString() === year) {
                    if (course.time === "MORNING") {
                        runInAction(() => {
                            courseday.coursemorning = course
                            course.color = "darkgray"
                            courseday.morning = course
                        })

                    } else {
                        runInAction(() => {
                            courseday.courselunch = course
                            course.color = "darkgray"
                            courseday.lunch = course
                        })
                    }
                }
            }
            weekcourses.push(courseday)

            i++;
        }

        runInAction(() => this.weeks = weekcourses)

        for (const semester of this.semesters) {
            if (semester.year.toString() === year) {
                runInAction(() => this.currentSemester = semester.courses)
            }
        }
    }

    async fetchWeeks() {


        let myHeaders = new Headers();
        myHeaders.append("Authorization", "Bearer " + localStorage.getItem("portal-jwt-Token"));

        let requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };

        await fetchData(() => fetch(config.url.API_URL + "/students/courses", requestOptions), (courses) => {
            runInAction(() => this.courses = courses)
            let years = [];
            let semesters = [];
            for (const course of courses) {
                if (!years.includes(course.year)) {
                    years.push(course.year)
                }
            }
            for (let i = 0; i < years.length; i++) {
                let semester = {
                    name: null,
                    year: null,
                    courses: []
                }
                semester.name = "Semester " + (i + 1);
                semester.year = years[i];
                for (const course of courses) {
                    if (course.year === years[i]) {
                        semester.courses.push(course)
                    }
                }
                semesters.push(semester);
            }
            runInAction(() => this.semesters = semesters)
        })

    }

}
