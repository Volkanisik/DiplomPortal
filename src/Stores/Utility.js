
async function fetchData(Fetchcall,callback){
    try {
        let response = await Fetchcall()
        const status = response.status
        if(status === 403){
            localStorage.clear()
            window.location.replace("/")
        }else if(response.ok) {
            const json = await response.json()
            callback(json)
        }else{
            alert(" Server not responding correctly. Data might be missing. ")
            console.log("status: "+status)
        }
    }catch (e) {
        alert(" Server not responding. Data might be missing. ")
        console.log("error: "+e.toString())
    }
}


export default fetchData