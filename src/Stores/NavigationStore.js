import {makeObservable, observable} from "mobx";

class NavigationStore {

    page = 0

    constructor() {
        makeObservable(this, {
            page: observable,
        })
    }

    updatePage(page) {
        this.page = page
    }
}

export const navigationStore = new NavigationStore()
