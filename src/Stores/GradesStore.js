import {makeObservable, observable, runInAction} from "mobx";
import {config} from "../Constants/Constants";
import fetchData from "./Utility";

class GradesStore {

    semesters = [];
    url1 = config.url.API_URL+"/students/grades"
    url2 = "http://localhost:8080/students/grades"

    constructor() {
        makeObservable(this, {
            semesters: observable
        })
    }

    async fetchTakes() {
        var token = 'Bearer ' + localStorage.getItem("portal-jwt-Token")
        var myHeaders = new Headers();
        myHeaders.append("Authorization", token);

        var requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };
        await fetchData(()=>fetch(this.url1, requestOptions),(result)=>{
            let years = []
            let studentSemesters = []
            for (const grade of result) {
                if(!years.includes(grade.year)){
                    years.push(grade.year)
                }
            }
            let i = 1;
            for (const year of years) {
                let semester = {
                    nr: i,
                    year: year,
                    courses: []
                }
                for (const grade of result) {
                    if(grade.year === semester.year){
                        semester.courses.push(grade)
                    }
                }
                studentSemesters.push(semester)
                i++;
            }

            runInAction(()=> this.semesters = studentSemesters);
        })
    }
}

export const gradesStore = new GradesStore()

