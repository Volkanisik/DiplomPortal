//https://a-carreras-c.medium.com/development-and-production-variables-for-react-apps-c04af8b430a5
const dev = {
    url: {
        API_URL: 'http://localhost:8080'
    }
};
const API = {
    url: {
        API_URL: process.env.REACT_APP_API_URL
    }
};
export const config = process.env.NODE_ENV === 'development' ? dev : API;