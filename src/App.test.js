import {act, render, screen} from '@testing-library/react';

import { unmountComponentAtNode } from "react-dom";
import Kurser from "./Components/Kurser";
import Karakter from "./Components/Karakter";
import Kalender from "./Components/Kalender";
import Kursus from "./Components/Kursus";
import {createData} from "./Components/CourseTable";
import CourseCard from "./Components/StudiePlanlægger/SemesterCourseCard"
import LogIn from "./LogIn";
import React from "react";
import { shallow } from 'enzyme';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import KurserList from "./Components/StudiePlanlægger/AllCoursesList";
import {DragDropContext} from "react-beautiful-dnd";
import SemesterCoursesList from "./Components/StudiePlanlægger/SemesterCoursesList";

Enzyme.configure({ adapter: new Adapter() });


let container = null;
beforeEach(() => {
  // setup a DOM element as a render target
  container = document.createElement("div");
  document.body.appendChild(container);
});


afterEach(() => {
  // cleanup on exiting
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});


it('Login page renders without crashing', ()=>{
  act(()=>{render(<LogIn />, container);});
  expect(screen.getByText('Login med Campus net')).toBeInTheDocument();
});



describe('<Kursus/>', () =>{
  it('Kursus 1234 renders without crashing', () => {
    const wrapper = shallow(<Kursus required={true} match={{params: {id: 1234}, isExact: true, path: "", url: ""}}
    />);
    expect(wrapper.containsMatchingElement(<h3 style={{textAlign: "left"}}>{1234}</h3>)).toBe(true)
  });
});


it('Test Semester Course card component with fake data',()=>{
  const fakeCourse = { id:1234, index: 0, name: "programming", day: "MON", time: "MORNING", ects:5  };
  act(() => {
    render(<CourseCard id={fakeCourse.id}
                       name={fakeCourse.name}
                       index={fakeCourse.index}
                       day={fakeCourse.day}
                       time={fakeCourse.time}
                       ects={fakeCourse.ects}
                       isNotDraggable={false}

    />, container);
  });
  //expect(container).toHaveProperty('backgroundColor', '#C4C4C4')
  expect(screen.getByText(fakeCourse.name)).toBeInTheDocument();
  expect(screen.getByText(fakeCourse.id)).toBeInTheDocument();
  expect(screen.getByText('5 ECTS')).toBeInTheDocument();
  expect(screen.getByText('MON kl.8')).toBeInTheDocument();

  /*This part tests if the Paper element is rendered correctly, with correct styles
  * This is also tests the condition of isNotDraggable = false, so the color will be changed.
  * */
  expect(screen.getByTestId('coursecard-element1234'))
  expect(screen.getByTestId('coursecard-element1234')).toHaveStyle(`backgroundColor: #C4C4C4`);
  expect(screen.getByTestId('coursecard-element1234')).toHaveStyle(`width: 300px`)
})

it('Tester KurserList komponent, hvis den viser korrekt data med korrekt elementer',()=> {
  const courseObject = (courseid, name, day, time, image, ects) => {
    let course = {
      id: courseid,
      name: name,
      day: day,
      time: time,
      image: image,
      ects: ects
    }
    return (course)
  }

  /*adding the temp data*/
  let data = []
  for (let i = 0; i < 5; i++) {
    let obj = courseObject(12+i,"name"+i,"MON",((i%2)===0)?"MORNING":"LUNCH",null,5)
    data.push(obj)
  }
  const handleDragEnd = (data) => {
    if (!data.destination) {
      return
    }else {
      console.log(data)
    }
  }

  act(() => {
    render(
        <DragDropContext onDragEnd={handleDragEnd}>
        <KurserList coursedata={data}/>
        </DragDropContext>
        ,container)
  })

  /*Test to test all the data is correctly shown by the component Course Card and the styles
  * of the card matches*/
  data.map((course)=>{
    expect(screen.getByText(course.name)).toBeInTheDocument();
    expect(screen.getByText(course.id)).toBeInTheDocument();
    expect(screen.getAllByText(/5 ECTS/i));
    expect(screen.getAllByText(/MON kl.13/i));
    expect(screen.getAllByText(/MON kl.8/i));
    expect(screen.getByTestId('coursecard-element'+course.id))
    expect(screen.getByTestId('coursecard-element'+course.id)).toHaveStyle(`backgroundColor: #C4C4C4`);
    expect(screen.getByTestId('coursecard-element'+course.id)).toHaveStyle(`width: 300px`)
  })

  /*Test to test the element with id "custom-element" has the correct style properties shown*/
  expect(screen.getByTestId('custom-element')).toHaveStyle(`backgroundColor: #ECECEC`);
  expect(screen.getByTestId('custom-element')).toHaveStyle(`borderRadius: 12px`);

})

it('Test SemesterCoursesList and SemesterCourseCard components ',()=> {
  const semesterCourseObject = (studentId, courseId, name, day, time, year, ect) => {
    let semester = {
      grade: -1,
      students_id: studentId,
      courses_id: courseId,
      name: name,
      year: year,
      day: day,
      time: time,
      ects: ect
    }
    return (semester)
  }
  let semesters = []
  let j = 0
  const handleDragEnd = (data) => {
    if (!data.destination) {
      return
    }else {
      console.log(data)
    }
  }
  Array.apply(null, Array(6)).map(function (x, index) {
    let semester = {
      year: (2018+index),
      index: index,
      courses: []
    }
    let year = 2018
    let temp = 0
    for (let i = j; i < (j+5); i++) {
      const obj = semesterCourseObject("id" + i, "course" + i, "DevOps" + i, "MON", ((i % 2) === 0) ? "MORNING" : "LUNCH", (year + 1),5)
      semester.courses.push(obj)
      temp = i
    }
    j = (temp+1)
    semesters.push(semester)
  })

  act(() => {
    {
      semesters.map((course)=>{
        render(
            <DragDropContext onDragEnd={handleDragEnd}>
              <SemesterCoursesList courses={course} index={course.index}/>
            </DragDropContext>
            ,container)
      })
    }
  })

  /* functions getByText and getByTestId will return error if duplicate/no element found, so this will test if all the
  * courses and semesters are presented as well as all the elements like <Paper> and <Box>*/
  const thisYear = new Date().getUTCFullYear()
  semesters.map((semester,index)=>{
    /*tests both value and the elements are presented*/
    expect(screen.getByTestId('semesterBox-element'+index))
    expect(screen.getByText((semester.index+1)+".Semester"))
    expect(screen.getByTestId('semesterPaper-element'+index)).toHaveStyle((`backgroundColor: #ECECEC`))  //if background color is same for all
    semester.courses.map((course)=>{
      expect(screen.getByText(course.name)).toBeInTheDocument();
      expect(screen.getByText(course.courses_id)).toBeInTheDocument();
      expect(screen.getAllByText(/5 ECTS/i));
      expect(screen.getAllByText(/MON kl.13/i));
      expect(screen.getAllByText(/MON kl.8/i));
      expect(screen.getByTestId('coursecard-element'+course.courses_id))
      if(semester.year <= thisYear){
        expect(screen.getByTestId('coursecard-element'+course.courses_id)).toHaveStyle(`backgroundColor: #D9D9D9`);
      }else {
        expect(screen.getByTestId('coursecard-element'+course.courses_id)).toHaveStyle(`backgroundColor: #C4C4C4`);
      }
      expect(screen.getByTestId('coursecard-element'+course.courses_id)).toHaveStyle(`width: 300px`)
    })
  })



})


it('Test Table', () => {
  var tableExpect = {
    "description": "Jenkins - Overblik Jenkins Blue Ocean Pipelines med Blue Ocean Build a Java app with Maven",
    "homework": "https://jenkins.io/doc/tutorials/build-a-java-app-with-maven/",
    "assignment": "https://jenkins.io/doc/book/blueocean/creating-pipelines/",
    "files": "https://www.jenkins.io/projects/blueocean/"
  }
  var tableActual = createData("Jenkins - Overblik Jenkins Blue Ocean Pipelines med Blue Ocean Build a Java app with Maven","https://jenkins.io/doc/tutorials/build-a-java-app-with-maven/","https://jenkins.io/doc/book/blueocean/creating-pipelines/","https://www.jenkins.io/projects/blueocean/")
  expect(tableExpect).toStrictEqual(tableActual)
});

it('Kalendar renders without crashing', () => {
  act(() => {    render(<Kalender />, container);  });
  expect(screen.getByText('Mandag')).toBeInTheDocument();
  expect(screen.getByText('Tirsdag')).toBeInTheDocument()
  expect(screen.getByText('Onsdag')).toBeInTheDocument()
  expect(screen.getByText('Torsdag')).toBeInTheDocument()
  expect(screen.getByText('Fredag')).toBeInTheDocument()
  expect(screen.getAllByText('08:00'))
  expect(screen.getAllByText('13:00'))
  expect(screen.getAllByText('12:00'))
});
it('Karakter renders without crashing', () => {
  act(() => {    render(<Karakter />, container);  });

});
it('Kurser renders without crashing', () => {
  act(() => {    render(<Kurser  />, container);  });
  expect(screen.getByText('Courses')).toBeInTheDocument();
});


